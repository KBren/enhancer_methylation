
#Dependent scripts
#MethylMix_No_filter

#######################
#Analyzing enhancer methylation HNSC locally
#############################

#Get the list of enhancers provided by Yao et al in the ELMER paper
Enhancers=read.table("~/Documents/Projects/Enhancer_meth/data/Enhancers_ELMER.txt", header=T, sep="\t")
Enhancers$chr2=gsub("chr","",Enhancers$chr)
rownames(Enhancers)=Enhancers$Probe  

Yao_Hyper=read.table("~/Documents/Projects/Enhancer_meth/data/Yao_diff_meth_ELMER_probes_Hyper.txt", header=T, sep="\t")
Yao_Hypo=read.table("~/Documents/Projects/Enhancer_meth/data/Yao_diff_meth_ELMER_probes_Hypo.txt", header=T, sep="\t")

#Yao hyper and hypo enhancners in CRC
Yao_Hyper_CRC=Yao_Hyper[Yao_Hyper$CT=="CRC",]
#3202
Yao_Hypo_CRC=Yao_Hypo[Yao_Hypo$CT=="CRC",]
#14098
#17300 altogether 

#############################
#Running MethylMix on server with function that does not apply filters 
#########################

#Note: I ran this on crosswood, as it takes ages to run
RscriptsPath="/home/kbren/rscripts/"
source("/home/kbren/rscripts/MethylMix_No_filter.R")

cancer="COADREAD"
METpath="/srv/gevaertlab/data/TCGA/Kevin_Data/MethylationData450k/"
EXPpath="/srv/gevaertlab/data/TCGA/Kevin_Data/GeneExpression/"

Meth=MethylMix_No_filter(cancer,METpath,EXPpath)
#This function automatically saves the results

#I then scp this over to local machine by running the following in terminal
#scp -r kbren@crosswood.stanford.edu:/srv/gevaertlab/data/TCGA/Kevin_Data/Full_MethylMix_No_filter_COADREAD.RData ~/Documents/Projects/Enhancer_meth/results/

#get MethylMix results
dat=load("~/Documents/Projects/Enhancer_meth/results/Full_MethylMix_No_filter_COADREAD.RData")  
MethylationStates_COADREAD=as.data.frame(MethResults$MethylationStates) 

MethylationDrivers_COADREAD=as.data.frame(MethResults$MethylationDrivers)
colnames(MethylationDrivers_COADREAD)[1]="IlmnID"
MethylationDrivers_COADREAD$IlmnID=as.character(MethylationDrivers_COADREAD$IlmnID)
rownames(MethylationDrivers_COADREAD)=MethylationDrivers_COADREAD$IlmnID

#Make variable indicating if an abnormally methylated CpG probe is among the list of enahncer CpGs provided by Elmer
MethylationDrivers_COADREAD$Enhancers=ifelse(rownames(MethylationDrivers_COADREAD) %in% rownames(Enhancers),2,1)

summary(as.factor(MethylationDrivers_COADREAD$Enhancers))
#There are 47372 enhancer sites identified by MethylMix

#Making matrix indicating the abnormally methylated CpG probes, and indicating the minimum hypomethylated and max hypermethylated states
#takes a while to run
MixtureStates_COADREAD=MethResults$MixtureStates
MethylationDrivers_COADREAD$hypoState=rep(NA, nrow(MethylationDrivers_COADREAD))
for(i in 1:length(MixtureStates_COADREAD)){
  MethylationDrivers_COADREAD$hypoState[i]=ifelse(min(unlist(MixtureStates_COADREAD[i]))<0,min(unlist(MixtureStates_COADREAD[i])),NA)
}
length(which(!is.na(MethylationDrivers_COADREAD$hypoState) & MethylationDrivers_COADREAD$Enhancers==2))
#37680 hypo enhancers in COADREAD
MethylationDrivers_COADREAD$hyperState=rep(NA, nrow(MethylationDrivers_COADREAD))
for(i in 1:length(MixtureStates_COADREAD)){
  MethylationDrivers_COADREAD$hyperState[i]=ifelse(max(unlist(MixtureStates_COADREAD[i]))>0,max(unlist(MixtureStates_COADREAD[i])),NA)
}
length(which(!is.na(MethylationDrivers_COADREAD$hyperState) & MethylationDrivers_COADREAD$Enhancers==2))
#11873 hyper enhancer CpGs in COADREAD

names(MixtureStates_COADREAD)=rownames(MethylationStates_COADREAD)
#N unimodal/multimodal

MethylationDrivers_COADREAD$N_States=rep(NA, nrow(MethylationDrivers_COADREAD))
for(i in 1:nrow(MethylationDrivers_COADREAD)){
  cg=rownames(MethylationDrivers_COADREAD)[i]
  MethylationDrivers_COADREAD$N_States[i]=length(MixtureStates_COADREAD[[cg]])
}

#Intersect, to identify the percentage of CpG sites identifed by ELMER that are also identied MethylMix 
length(intersect(rownames(MethylationDrivers_COADREAD[!is.na(MethylationDrivers_COADREAD$hyperState),]), Yao_Hyper_CRC$Probe))
round(3156/3202,2)
#99% of Elmer enhancer CpGs that Yao found to be hypermethylated in COADREAD are also found to be hypermethylated by MethlMix

length(intersect(rownames(MethylationDrivers_COADREAD[!is.na(MethylationDrivers_COADREAD$hypoState),]), Yao_Hypo_CRC$Probe))
round(14018/14098,2)
#99% of Elmer enhancer CpGs that Yao found to be hypomethylated in COADREAD are also found to be hypomethylated by MethlMix

#find out frequency of abnormal methylation states within patient population 
MethylationDrivers_COADREAD$Nhyper=rep(NA, nrow(MethylationDrivers_COADREAD))
MethylationDrivers_COADREAD$Nhypo=rep(NA, nrow(MethylationDrivers_COADREAD))
for(i in 1:length(MixtureStates_COADREAD)){
  MethylationDrivers_COADREAD$Nhyper[i]=ifelse(max(unlist(MixtureStates_COADREAD[i]))>0,length(which(MethylationStates_COADREAD[i,]>0)),NA)
  MethylationDrivers_COADREAD$Nhypo[i]=ifelse(min(unlist(MixtureStates_COADREAD[i]))<0,length(which(MethylationStates_COADREAD[i,]<0)),NA)
  
}
summary(MethylationDrivers_COADREAD$Nhyper)
summary(MethylationDrivers_COADREAD$Nhypo)

save(MethylationDrivers_COADREAD, file="~/Documents/Projects/Enhancer_meth/results/MethylationDrivers_COADREAD.RData")
dat=load("~/Documents/Projects/Enhancer_meth/results/MethylationDrivers_COADREAD.RData")

######################################
#Now use aclust to identify enhancer CpG clusters, so that in subsequent analyses, we're studying each event independently, rather than having multiple correlated events for eah enahncer
######################################

#just running aclust on abnormally methylated CpGs enhancer CpGs probes, as aclust takes forever to run

#set treshold for correlation of adjacent CpGs in CpG clusters
tresh=0.3 #Pearson correlation >0.7
bp=10000  

#getting the data from the crosswood
#scp ~/Documents/Projects/Enhancer_meth/results/MethylationDrivers_COADREAD.RData kbren@crosswood.stanford.edu:/srv/gevaertlab/data/TCGA/Kevin_Data/
#get METcancer and METnormal data for all of the abnormally methylated enhancer probes

#scp -r kbren@crosswood.stanford.edu:/srv/gevaertlab/data/TCGA/Kevin_Data/METData_enhancer_COADREAD.RData ~/Documents/Projects/Enhancer_meth/data/
     
dat=load("~/Documents/Projects/Enhancer_meth/data/METData_enhancer_COADREAD.RData")

METcancer=METData_enhancer_COADREAD$METcancer
METnormal=METData_enhancer_COADREAD$METnormal

probes=rownames(MethylationDrivers_COADREAD[MethylationDrivers_COADREAD$Enhancers==2,])
METcancer=METcancer[probes,]

RscriptsPath="~/Documents/AllRscripts/rscripts/"
source(paste(RscriptsPath,"TCGA_Pancancer_Scripts_KB2.R",sep=""))

Sclust=assign.to.clusters.multi.chr(METcancer, tresh)

####################################
#make bedgraph files for hyper and hypo CpG probe MethylMix DM values, so that we can add them as custom tracks on UCSC genome browser. 
####################################

RscriptsPath="~/Documents/AllRscripts/rscripts/"
source(paste(RscriptsPath,"TCGA_Pancancer_Scripts_KB2.R",sep=""))

MixtureStates=MethResults$MixtureStates
names(MixtureStates)=rownames(MethResults$MethylationStates)
probes=rownames(MethylationDrivers_COADREAD[MethylationDrivers_COADREAD$Enhancers==2,])

bed=make.bedgraph.DM.state(MixtureStates, probes)

write.table(bed$hyper, file="~/Documents/Projects/Enhancer_meth/Ehancer_hyper_bed.DM.states.txt", sep="\t", row.names = FALSE, col.names = FALSE, quote = FALSE)
write.table(bed$hypo, file="~/Documents/Projects/Enhancer_meth/Ehancer_hypo_bed.DM.states.txt", sep="\t", row.names = FALSE, col.names = FALSE, quote = FALSE)

#You need to manually add some header lines in textedit (or code it using linux) for these to function as bedgraph files

#browser position chr11:22647309-22647430 #can make this any position
#browser hide all
#browser pack refGene encodeRegions
#track type=bedGraph name="max DM val hyper" description="DM value" visibility=full color=0,0,1 autoScale=off viewLimits=-0.8:0.8 yLineMark=1 yLineOnOff=on priority=20 Track height=500

#Note, for that last one, it should be hypo for the other track

######################################
#Now find the probes that have an enhancer chromatin state in three primary cell types from the 111 epigenomes study (Epigenomics roadmap)
#Two colon musoca and one rectal mucosa
###########################################

#First download all of the files form the 111 reference epigenomes study
#doing this programatically 

library(XML)
library(RCurl)

url="http://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/"
urlData=getURL(url)
urlData2=unlist(strsplit(urlData,"\\n")) # split into individual lines 

report=as.matrix(urlData2[grep("_15_coreMarks_dense.bed",urlData2)])
report=report[-grep(".tbi|.bgz", report)] 
report=unlist(strsplit(report, ">|<"))
report=report[grep("_15_coreMarks_dense.bed",report)]
report=report[-grep("a href",report)]

dir="~/Documents/Projects/Enhancer_meth/data/ReferenceEpigenomes"
dir.create(dir)
setwd(dir)

#scrape filenames from epigenomics roadmap website
url="http://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/"
#FileTypeIDs=paste(FileID, FileType,".gz",sep="")
link=paste(url,report, sep="")
for(i in 1:length(link)){
     wget_string=paste("wget nc --user=" , " ", link[i],   sep="")
     system(wget_string, intern=TRUE)
}

#all downloaded
files=list.files(dir)
files=files[grep(".gz",files)]
#files=paste(dir,"/",files[grep(".gz", files)], sep="")
for(i in 1:length(files)){
     file=as.character(files[i])
     if(file %in% files){
          R.utils::gunzip(file)
     } else {
          print("File does not exist")
     }     
}

#Now make file annotating the chromatin states for all 450k array probes for all of the cell types
library(IlluminaHumanMethylation450kanno.ilmn12.hg19)
ann450k=getAnnotation(IlluminaHumanMethylation450kanno.ilmn12.hg19)

#relevant files for colonic mucosa
#E075 colonic mucosa
#E101 rectal mucosa
#E102 rectal mucosa

#I have downloaded all of the files from the 111 epigenomes paper
dir="~/Documents/Projects/Enhancer_meth/data/ReferenceEpigenomes"
files=list.files(dir)

#make granges object from the annotation file
dat2=ann450k[,c("chr","pos","pos","Name")]
colnames(dat2)=c("chr","start","end")
dat.gr=makeGRangesFromDataFrame(dat2[,c(1:3)])
values(dat.gr)=dat2[,c(4:ncol(dat2))]

#Making dataframe with chromatin states from the 111 epigeneomes study for all CpGs on the 450k array
IDs=gsub("_.*","",list.files(dir))
chrom.states=list()
for(i in 1:length(IDs)){
     ID=IDs[i]
     file=files[grep(ID, files)]
     file=paste(dir, "/", file, sep="")
     cm=read.table(file, sep="\t", skip=1)
     colnames(cm)=c("chr","start","end","state","NA1","NA2","start2","end2","col")
     cm.gr=as.data.table(cm)
     cm.gr2=makeGRangesFromDataFrame(cm.gr[,1:4])
     values(cm.gr2)=cm.gr$state
     names(mcols(cm.gr2))="state"
     #cm.gr2=dropSeqlevels(cm.gr2, "chrX")
     #cm.gr2=dropSeqlevels(cm.gr2, "chrY")
     cm.gr2=dropSeqlevels(cm.gr2, "chrM")
     
     #make an object that shows the state for each CpG
     ranges=subsetByOverlaps(cm.gr2, dat.gr)
     hits <- findOverlaps(cm.gr2, dat.gr)
      
     idx <- subjectHits(hits)
     idy <- queryHits(hits)

     values <- DataFrame(probe=dat.gr$X[idx], state=cm.gr2$state[idy])
     #this gives the state associated with each CpG
     values=as.data.frame(values)
     #rownames(values)=values$probe
     chrom.states[[i]]=values
}
names(chrom.states)=IDs
ann450k.ER111=array(NA,c(nrow(ann450k),length(chrom.states)))
rownames(ann450k.ER111)=rownames(ann450k)
colnames(ann450k.ER111)=names(chrom.states)
for(i in 1:ncol(ann450k.ER111)){
     ann450k.ER111[chrom.states[[i]][,1],i]=as.character(chrom.states[[i]][,2])
}
ann450k.ER111=as.data.frame(ann450k.ER111)
ann450k.ER111=cbind(as.data.frame(ann450k),ann450k.ER111)
save(ann450k.ER111, file="~/Documents/Projects/Enhancer_meth/data/annotation.450k.ER111Study.RData")

dat=load("~/Documents/Projects/Enhancer_meth/data/annotation.450k.ER111Study.RData")

#############################################
#Now identify the abnormally methylated CpG probes that are annotated as within one of the three enahncer categories in all the three colonic and rectal mucosa data sets
###############################################

#E075 colonic mucosa
#E101 rectal mucosa
#E102 rectal mucosa

MethylationDrivers_COADREAD=cbind(MethylationDrivers_COADREAD, ann450k.ER111[rownames(MethylationDrivers_COADREAD),c("E075","E101","E102")])

high.conf.enh=with(MethylationDrivers_COADREAD, MethylationDrivers_COADREAD[ E075 %in% c("7_Enh","12_EnhBiv","6_EnhG") & E101 %in% c("7_Enh","12_EnhBiv","6_EnhG") & E102 %in% c("7_Enh","12_EnhBiv","6_EnhG"),])
summary(as.factor(high.conf.enh$Enhancers))
#1385/2759 (50%) labelled as enahncers by Elmer paper. Not sure why half of them were not. 


#for these colorectal mucosa enahncers, what proportion were identified by Elmer versus MethylMix
high.conf.enh.hypo=high.conf.enh[!is.na(high.conf.enh$hypoState) & high.conf.enh$Enhancers==2,]
#834

length(intersect(Yao_Hypo_CRC$Probe, high.conf.enh.hypo$IlmnID))
#Elmer identified 197/834 (24%) of these hypo CRC enhancers


high.conf.enh.hyper=high.conf.enh[!is.na(high.conf.enh$hyperState) & high.conf.enh$Enhancers==2,]
#667 sites

length(intersect(Yao_Hyper_CRC$Probe, high.conf.enh.hyper$IlmnID))
#Elmer identified 140/667 (20%) of these hyper CRC enhancers

#What's the difference between the sites that Elmer identifies versus those it doesn't? 
#are the ones it fails to detect lower frequency events? 

boxplot(
high.conf.enh.hyper[intersect(high.conf.enh.hyper$IlmnID,Yao_Hyper_CRC$Probe),"Nhyper"],
high.conf.enh.hyper[setdiff(high.conf.enh.hyper$IlmnID, Yao_Hyper_CRC$Probe),"Nhyper"])

boxplot(
     high.conf.enh.hypo[intersect(high.conf.enh.hypo$IlmnID,Yao_Hypo_CRC$Probe),"Nhypo"],
     high.conf.enh.hypo[setdiff(high.conf.enh.hypo$IlmnID, Yao_Hypo_CRC$Probe),"Nhypo"])

boxplot(
     high.conf.enh.hyper[intersect(high.conf.enh.hyper$IlmnID,Yao_Hyper_CRC$Probe),"hyperState"],
     high.conf.enh.hyper[setdiff(high.conf.enh.hyper$IlmnID, Yao_Hyper_CRC$Probe),"hyperState"])

boxplot(
     high.conf.enh.hypo[intersect(high.conf.enh.hypo$IlmnID,Yao_Hypo_CRC$Probe),"hypoState"],
     high.conf.enh.hypo[setdiff(high.conf.enh.hypo$IlmnID, Yao_Hypo_CRC$Probe),"hypoState"])

par(mfrow=c(1,2))
stripchart(high.conf.enh.hypo[intersect(high.conf.enh.hypo$IlmnID,Yao_Hypo_CRC$Probe),"hypoState"], vertical=T, method="jitter", ylim=c(-0.7, -0.05))
stripchart(high.conf.enh.hypo[setdiff(high.conf.enh.hypo$IlmnID,Yao_Hypo_CRC$Probe),"hypoState"], vertical=T, method="jitter", ylim=c(-0.7, -0.05))

#the hypo and hyper probes identified by MethylMix but not ELMER tend to be less frequent (as we might expect), less hypo or hyper, compared with those that are also identified by ElMER. 
#This is also unsurprising, but I hope that MethylMix is not only doing better than ELMER because we set a lower threshold for differential methylation (like 10% rather than 20%)
#Need to investigate further. 

########################################
#function to get names of genes within threshold distance of a given enhancer CpG
#This needs work, but the basic pronicple is right
##########################################

LoadFile=paste("~/Documents/Projects/TCGA_COADREAD/data/2016-08-04/MA_COADREAD_Processed.Rdata")
load(LoadFile)

MAcancer <- ProcessedData[[1]]
#520 samples
MAnormal <- ProcessedData[[2]]
#4 samples
rm(ProcessedData)

colnames(MAcancer)=substr(colnames(MAcancer),1,12)
colnames(MAnormal)=substr(colnames(MAnormal),1,12)

pr=c("cg13205050")
GetnearbyGenes(pr, 100000, 100000)








